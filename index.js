// bai tập 1 ------------
    function tinhLuong(){
        const ngayCong = document.getElementById('txt-ngay-lam').value *1;
        const luongNgay = document.getElementById('txt-luong-ngay').value *1;
        var tienLuong = ngayCong * luongNgay;
        document.getElementById("result").innerHTML = `Tiền Lương: ${tienLuong}`;
    }
// bài tập 2 ------------
    function tinhTrungBinh(){
        const num2 = document.getElementById('txt-so-2').value *1;
        const num1 = document.getElementById('txt-so-1').value *1;
        const num3 = document.getElementById('txt-so-3').value *1;
        const num4 = document.getElementById('txt-so-4').value *1;
        const num5 = document.getElementById('txt-so-5').value *1;
        var trungBinh = (num1 + num2 + num3 + num4 + num5)/5
        document.getElementById("result-2").innerHTML = `Giá trị trung bình ${trungBinh}`;
    }
// bài tập 3 ---------
    function doiTien(){
        const tyGia = 23500;
        const tienDo = document.getElementById('txt-tien-do').value *1;
        var tienViet = tienDo*tyGia;
        document.getElementById("result-3").innerHTML = `Tiền Việt: ${tienViet}`;
    }
    // bài tập 4 -------
    function tinhHCN(){
        const chieurong = document.getElementById('txt-chieu-rong').value *1;
        const chieudai = document.getElementById('txt-chieu-dai').value *1;
        var chuVi = (chieudai + chieurong) *2;
        var dienTich = chieudai * chieurong;
        document.getElementById("result-4").innerHTML = `Chu vi: ${chuVi}, Diện tích: ${dienTich}`;
    }
    // Bài tập 5 ---------
    function tinhTong(){
        const soA = document.getElementById('txt-so').value *1;
        var donVI = soA %10;
        var chuc = Math.floor(soA/10);
        var tong = donVI + chuc;
        document.getElementById("result-5").innerHTML = `tổng ký số: ${tong}`;
    }